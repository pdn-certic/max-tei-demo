<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:tei="http://www.tei-c.org/ns/1.0" 
version="2.0" 
exclude-result-prefixes="tei xsl">

	<xsl:output method="xml" encoding="utf-8"/>

	<xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>

</xsl:stylesheet>
