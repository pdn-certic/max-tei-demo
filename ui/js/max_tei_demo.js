
document.addEventListener('DOMContentLoaded',() =>{
    document.querySelectorAll('.row a').forEach((elt) => {
        elt.addEventListener('mouseover', () => {
            elt.parentElement.querySelectorAll('.demo_text').forEach((dt) => {
                dt.style.display = 'block';
            })
        });

        elt.addEventListener('mouseout', () => {
            elt.parentElement.querySelectorAll('.demo_text').forEach((dt) => {
                dt.style.display = 'none';
            })
        })
    })
    
});
