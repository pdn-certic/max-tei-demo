declare default element namespace "http://www.tei-c.org/ns/1.0";
declare variable $baseURI external;
declare variable $dbPath external; (:path du document dans la db:)
declare variable $project external; (: id du projet :)
declare variable $doc external; (: nom du document :)

(: xquery listant les items du sommaire du document $doc:)
<ul>
  <li id="..." data-href="..."><head>test</head></li>
  [ ... ]
</ul>
